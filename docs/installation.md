# Setting up this repository

To get full functionality out of the programs contained a number of setup
steps are required.

A custom version of pyDIA for use with this project is available at
https://bitbucket.org/cordwella/pydia/src/master/

Briefly the installation instruction are as follows:
- Install CUDA
- Setup iraf via astroconda using the legacy software stack (see: https://astroconda.readthedocs.io/en/latest/installation.html#iraf-install)
- inside the astroconda enivorment install the pyDIA required libaries using pip (pycuda scikit-learn scikit-image)
- clone pyDIA to a folder of your choice
- inside the pyDIA Code directory run 'make GPU_double'. This completes pyDIA installation
- clone this repo to a folder of your choice
- install the rest of the required python libraries with 'pip install -r requirements.txt' from inside this repos main directory
- setup iraf by running 'mkiraf' in the directory you will be calling these scripts from
- Setup settings.py, required folder and download required files (see below)
- Update your python path to include this repository. (E.G. 'export PYTHONPATH="$(pwd)"'
if you are currently inside this directory. This can also be done inside any
scripts which might use this code by using sys.path.append('/path/to/repo'))



pyDIA uses IRAF to find star positions and compute the initial PSF,
in each directory you call a script that callss iraf functions run mkiraf (iraf can
then be accessed with cl, or within scripts)

The required libaries that are installed with requirements.txt should be (
if that file has an issue for whatever reason): requests, boto, ccdproc,
astroquery, astropy, pyds9, pandas, numpy, matplotlib

#### Modify the settings for your file path an moa database access credentials
A file called settings.py will need to be placed into the moa_asteroids
directory, this contains your access credentials for the MOA database. Using
read only credentials is advised.

The file should look like this::
    PARPATH = '/home/ubuntu/Documents/finalcode/par'

    # Data save path
    DATAP = '/home/ubuntu/Documents/finalcode/data'

    # Photometric Calibration File path
    CALIBP = '/home/ubuntu/Documents/finalcode/photcalib'

    PYDIA_DIR = '/home/ubuntu/Documents/pyDIA'
    ACCESS_KEY = ''
    SECRET_KEY = ''
    BUCKET = 'moa'

    at_uoa_campus = False

    if at_uoa_campus:
        S3_HOST = 'object.auckland.ac.nz'
    else:
        S3_HOST = 'objectext.auckland.ac.nz'

#### Setup required folders

Your datapath has to have a number of specifically named folders in it for it
to function. Inside that directory run these commands to set them up.

```
touch data/darks.dat
touch data/flats.dat

mkdir data/dark
mkdir data/dark_master
mkdir data/flat
mkdir data/flat_master

mkdir data/asteroids
mkdir data/ref

mkdir data/comp_stars
mkdir data/info

mkdir data/export
```


#### Download Required Files
INFO files from the MOA database need to be downloaded locally before the
scripts in this repository can be run, and the data directory structure
will need to be setup.

To download the INFO file I advise using some of the scripts from here: https://bitbucket.org/cordwella/moa-db-scripts,
particulally download_list. The path in the MOA_DB to download those files from is: info/GB
The file structure for these will need to then be flattened. Dark and Flat info
files also need to be downloaded.

Reference images for difference photometry will need to be downloaded to DATAP/ref/.
These files are not yet publically avaliable, however Ian Bond has access to all of them.
This folder will be updated with cropped copies of the reference frames for each subframe.

To include lightcurve calibration the moa photocalib files are also required. These files
include instrumental magnitudes of possible reference stars, and a mapping from those
instrumental magntiudes to Cousins I magnitudes (calcluated from OGLE data).
