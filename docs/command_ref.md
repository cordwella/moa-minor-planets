# Reference for induvidual commands

Reference for commands not included in README.md.

Not currently exaustive.

### Tracking an asteroid
Tracking an asteroid is almost the exact same as in Ian Bonds original code,
seen here https://bitbucket.org/iabond/asteroids. I advise using his codes example.

If you are wishing to track a number of asteroids I advise using the batch script.
Simply edit the bottom of the file to include the asteroid IDs you care about.

To manually track a single asteroid first generate the empheridies and then track
it through the field
```
cd moa_asteroids
python tracking/make_ephem.py {asteroid_id}
python tracking/track_in_moa.py {asteroid_id}
```
The tracking file will be saved as DATAP/asteroids/{asteroid_id}/track.dat

The track can then be viewed with
```
python trackng/draw_track.py {asteroid_id}
```

### Generating Statistics for asteroids

After tracking a single, or a number of asteroids statistics around how many
frames that it appears in and the number of passes. `python stats.py {asteroid_id}`
will print out the statistics for that asteroid and save it to a file (DATAP/asteroids/{asteroid_id}/stats.txt).


### Downloading passes that an asteroid appears in
This will download all source images of the asteroid.

```
cd moa_asteroids
python download_frames.py asteroid_id
```
The files will be saved in DATAP/asteroids/asteroid_id/source

### Running CCD Processing
Prerequsites: CCD frames for an asteroid downloaded to data/asteroids/{asteroid_id}/source
Flat and dark info files saved to data/info. Python enviroment with astroconda and
boto setup

Will output processed images to asteroids/{asteroid_id}/processed, and write
to darks.dat matching dark images and to flats.dat matching flat images. Master
dark and flat images will be created in data/dark_master and data/flat_master.
Flat and dark images will also be downloaded to data/dark and data/flat respectively
these directories can be cleared afterwards.

``` python process.py {asteroid_id}
```

#### Subframes

Each downloaded image will be cropped into a subframe of 512 x 512 pixels,
with aside from subframes at the edge of the original frame will also have a
a border added incase the asteroid is on the edge of a frame. The subframe names
take the form of '{row}_{column}' with both numbers starting at zero and
starting from the bottom left of the frame.

The subframe size can be changed in constants.py, however if this occurs it is
advisible to also update the subframe naming in util.py.


### Generating difference images for an asteorid

```
python photometry/make_difference_images.py {asteroid_id}
```

The difference images and all of their outputs will be placed into
DATAP/asteroids/{asteroid_id}/difference. See the pyDIA documentation for
details on what these files contain.

### View generated difference images and generate a list of asteroid positions
Due to slight differences in pointing and uncertainty in asteroid positions the
calculated position of the asteroid from the empheridies may not be the exact
position of the object. Addionally poor quality images, or images where the
minor planet is next to a saturated star will need to be removed
from the set used to calculate the lightcurve.

```
python photometry/positions.py {asteroid_id}
```

This can also be run manually by appending 'm' to the above command.

If you get an error and ds9 doesn't pop up you may need to download the ds9
exceutable and start it yourself.

### Perform photometry on the asteroid
Prerequsites: Difference images have been generated and a positions file
has been created

```
python photometry/perform_photmetry.py {asteroid_id}
```

The lightcurve will be saved in data/asteroids/{asteroid_id}/difflightcurve.csv.

If not enough valid comparison stars are avaliable the magnitudes will not be calculated.
Valid comparison stars are generated by comparing the predicted uncertainty of the
star with the difference flux from pyDIA, reasons for bad comparison stars are typically
poor difference imaging, poor intial images (rotator offset, clouds), or stellar variablity.

This will also generate in the difference directory fullmosaic.fits and fullpmosaic.fits
displaying cutouts of the asteroid in the difference images, before and after the psf
has been fitted to it respectively.

### (DEPRECATED) Calibrate the photometry on the asteroid
pyDIA will photometrically scale images to the reference but different
references will have a different base of photometry. This process is a
part of the standard photometry computation by default, but can also be done
on it's own.

To setup the photometric calibration the MOA-Red to Cousins I calibration
coefficents need to be in the photcalib directory, with the structure:
    photcalib/gb{}/moa-ogle-gb3-R-2.coef

```
python calibrate_lightcurve.py {asteroid_id}
```
### (DEPRECATED) Reduce a lightcurve to account for changes in orbital geometry

This will generate a file that should be used for any computation of rotation
period or shape. Deprecated as most asteroids will not have known phase
parameters.

```
python analysis/correct_lc.py {asteroid_id}
```

An additional version of this file with increased uncertainties can be
generated from
```
python analysis/update_uncertainties_estimate.py
```

### View the lightcurve graph (I magnitudes)

If the period is unknown
```
python analysis/lightcurve_graphs.py {asteroid_id}
```

Timewrapped with a known period (in hours)
```
python analysis/lightcurve_graphs.py {asteroid_id} {period}
```

## Notes on automated steps
### Automated checking of dark and flat frames
CCD Processing is now automated. The test for dark frames is for the mean of
128x128px subframes to be within 5% of the overall mean, for flats this
requires within 20% of the image mean.

This is run in check_dark_and_flat_frame.py.

Tests for this are also containted inside tests/test_dark_flat_check.py
On your first run you will need to run this with the additional parameter
'download' to download the test files.
(`python tests/test_dark_flat_check.py download`)

### Automated checking of registered subframes
Checking of subframes (after difference imaging) is now automated.

The checks that take place are:
- Checking for the asteroid position being within 3 pixels of a saturation mask
- Checking that the asteroid position is not too close to image edges
- Overall image goodness check determined by the number of detected stars in
the image being at least 40% of the detected stars in the reference image

Tests are also written for this, see
 moa_asteroids/tests/test_image_goodness_check.py
