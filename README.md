# Moa Minor Planets Scripts

Author: Amelia Cordwell<acor102@aucklanduni.ac.nz>

An intergrated repository for my MOA minor planet photometry software,
includes tracking, ccd processing and difference imaging photometry.

Currently due to the python 2 requirements of iraf/pyraf and pyDIA as a whole repo
this only runs on python 2.7 however most of the scripts should also run under
python3.

Tested on Ubuntu Linux (18.04) only.

## Functionality of this repository

This repository is capable of taking a known asteroid id, tracking it through
MOA exposures (given INFO files), extracting the exposures it is in from the
MOA object store database, displaying these images, performing CCD processing
on them and then doing difference image photometry on the images, to generate
a lightcurve. The lightcurve will then be calibrated to cousins I magnitudes.
(This is an approximate calibration)

During processing and before generating the difference images the frames
will be split 512 (+ BORDER) x 512 ( + BORDER) subframes.

This will only track and perform photometry on galatic bulge R band exposures.

Please see docs/ for more detailed documentation including installation
instructions.

## Parts of this repository

tracking/
an adapted part of Ian Bond's MOA asteriod tracking scripts, this will
generate datafiles tracking a given asteriod through MOA exposures and generate
a list of exposures and where to find the asteriod in a given exposure.

Note that this set of codes adapts upon the orginal code to give x, y cooridinates
in a specific subframe, this is appended on to the original generated
output into the files

processing/
download flats and darks for a given moa exposure, check theses for goodness
and then process downloaded fits files, also crops downloaded frames to subframes

photometry/
Uses Micheal Albrow's pyDIA code to perform difference imaging photometry
on images for an asteroid. First creates difference images, then does basic
checking on them, then performs photometry on the difference image and
finally converts that to I magnitudes. If during conversion not enough
good comparison stars are found it will ignore that frame.


## Recommended Use Instructions
The below files are reccomened for core usage of this repository, however
more detailed instructions of each file and the orders in which to run
them are in docs/command_ref.md

For general use it is reccomened to use the batch.py script to complete all
of the processing. Any time you see ```{name}``` in these documents replace
the whole thing including the brackets with whatever is named.

### batch.py

This is the reccomended way of using the pipeline for asteroids or a group
of asteroids. It is reccomended to edit the bottom of the code to change
the asteroids to run for and which steps to apply to them, and then run the
script with 'python batch.py'. E.g.

```
batch([157], track=True,  download=True, process_=False,
      difference_photometry=False, export_=False)
```

Further detail on the options are described at a docstring at the top of the
function. Each option (tracking, image donwloading, ccd processing, difference
imaging & photometry, generation of final export file) has the previous option
as a prerequsites.

There is also an option to present batch statistics on a set of asteroids. This
will display statistics about their tracking data only, and will fail if you
try to include asteroids that tracking has not been completed for. e.g.

```
batch_stats([str(i) for i in range(2000, 2500)])
```

These can also be run from a python terminal by importing

```
from moa_asteroids.batch import batch, batch_stats
```


### view_in_ds9.py
Use this script to view the track of an asteroid through images.

```
python view_in_ds9.py {asteroid_id}
```

By default this will display cropped subframes. If 'n' is placed as an additional
parameter it will display source subframes, similarly 'r' will give registered
subframes (created during difference imaging).

### status.py

Display the current status of all asteroids in the asteroid folder, plus the
number of possible exposures and the number of downloaded source exposures.

```
python status.py
```

Can be run with an asteroid id as a final parameter. The status for only that
asteroid will then be displayed.

The possible statuses are as follows

- No frames (in tracking) NO_MATCH
- Tracking complete but no other actions have been taken TRACK_COMPLETE
- Tracked frames no images MATCH_NO_IMAGES
- Images downloaded SOURCE_IMAGES
- Images have preprocessing PROCESSED_IMAGES
- Difference imaging carried out  HAS_DIFFERENCE_IMAGES
- Photometry Carried out LIGHTCURVE_CREATED
- Export file created EXPORT_COMPLETE


### photometry/manage_error.py

During the photometry process there will sometimes be errors. These are
typically trigged by a pyDIA segmentation fault. To manage this the files
corresponding to the subframe inside data/asteroids/{asteroid_id}/difference
should be deleted and the code for the subframe should be added to the
BAD_SUBFRAMES dict inside constants.py.  Once these steps are completed
difference imaging can continue from which ever script you are using.
(In the case of batch.py, now is a good time to run it with only
difference_photometry and export_ as True).

'manage_error.py' helps to automate these steps. It will identify the last
run set of exposures and offer to delete all difference imaging files related
to this set (exposures are run in groups that have the same field,
chip and subframe). It will then print out the field, chip and subframe tuple
to be added as a key to the BAD_SUBFRAMES dictionary.

(This is required as pyDIA creates a number of files it will later check for
and will get confused if those files are still there for a different set of
images.)


## Output Files
All output and intermediate files are stored in 'DATAP' as set in your settings
file. The documentation above below assumes ou set it as data/ off the current
directory.

### Asteroid Folders
Inside data/asteroids is a directory for each asteroid that has been tracked.
data/asteroids/{asteroid_id}/source will contain all downloaded source
exposures from the database.
data/asteroids/{asteroid_id}/processed will contain the cropped and ccd
processed set of images.
data/asteroids/{asteroid_id}/difference will contain the pyDIA output and
difference images for that asteroid. Each set of images will have
'{field}_{chip}_{subframe}' appended onto the end of the filename.

ephem.dat contains nightly empheridies for the asteroid between 2005 and 2010.
track.dat contains tracking of where the asteroid is in MOA exposures.
    Format: frame name, exposure jd, exposure time, RA, DEC, x position on
        original frame, y position on original frame, subframe name,
        x position on subframe, y position on subframe, earth asteroid distance (au),
        earth sun distance (au), phase angle (alpha, in degrees), expected V band
        magnitude
positions.dat asteroid positions on difference images after appling a
        centering algorithm and checks for edges and crossover with saturated
        pixels
    Format: frame, x position, y position, filename
difflightcurve.csv output lightcurve from difference images
    Format: julianday, frame, flux, fluxerr, instrumental mag,
            instrumental mag error, I band mag, I band mag error


### Export Files

At the end of processing one export file is generated for each asteroid.

Filename: data/export/{asteroid_id}_out.csv

Each line represents a possible exposure as identified by the INFO files. As
such not all lines will have all of the photometry information and will have
blank spaces where those are expected to be instead.

From left to right each line contains:
Frame ID (e.g)
Time of exposure in JD
exposure time in seconds
Right Ascention in degrees
Declination in degrees
X position on the original frame
Y position on the original frame
Subframe name ID
X position on the subframe
Y position on the subframe,
Whether or not photometry was completed
Time at asteroid (exposure time adjusted for light travel time) in JD
Delta/Earth asteroid distance in AU
R/Asteroid sun distance in AU,
Phase angle (alpha) in degrees
Cropped frame ID
Flux in counts
Flux error
Instrumental Magntiude
Instrumental Magnitude error
On sky I Magnitude
On sky I Magntiude error


### Misc
If you are running low on disk space the contents of dark, flats, dark_master
and flat_master can all safely be deleted, with the only caveat being that
certain files from here may have to be redownloaded with the next asteroid that
has images in that time period.


## Example Usage
If you are wanting to test out the output of this software try running for
asteroids 157 or 2043.
