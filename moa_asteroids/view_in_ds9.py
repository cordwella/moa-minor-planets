#! /usr/bin/env python

import sys
import pyds9
import os

from moa_asteroids.constants import (SOURCE_EXP_DIR, TRACK_FILENAME,
                                     PROCESSED_SUBF_FILENAME,
                                     REG_SUBF_FILENAME)

CARE_ABOUT_FIELD = False


def view_in_ds9(astid, startrun=None, subf=True, registered=True):
    """
    Display all downloaded exposures of a tracked asteroid with
    display of predicted position in the program ds9.

    arguments:
    - astid: id of the asteroid to display
    - startrun (optional): id of the run to start with. If given will only
      display images from the same chip field combination, only relavant if
      CARE_ABOUT_FIELD is True
    - subf (optional, default True): display cropped subframes or full images
    - registered (optional, default True): display registered images, or
      just the subframes with ccd processing (only used if subf is also True)
    """

    ftrack = open(TRACK_FILENAME.format(asteroid_id=astid))
    print(startrun)
    viewer = pyds9.DS9('ds9')
    found_start = False
    field = None
    chip = None
    frames = []
    for line in ftrack:
        if subf:
            frame, _, _, _, _, _, _, subf, x, y = line.split()[:10]
            if registered:
                filename = reg_subf_frame_file_name(frame, astid, subf)
            else:
                filename = subf_frame_file_name(frame, astid, subf)
        else:
            frame, _, _, _, _, x, y = line.split()[:7]
            filename = frame_file_name(frame, astid)

        if not CARE_ABOUT_FIELD:
            if os.path.isfile(filename):
                frames.append([filename, float(x), float(y)])
            else:
                print("Frame does not exists at {}".format(filename))
        else:
            if get_num(frame) == startrun:
                found_start = True
                chip = get_chip(frame)
                field = get_field(frame)
                filename = frame_file_name(frame, astid)
                if os.path.isfile(filename):
                    frames.append([filename, float(x), float(y)])
                else:
                    print("Frame does not exists at {}".format(filename))

            elif (found_start and chip == get_chip(frame)
                  and field == get_field(frame)):
                filename = frame_file_name(frame, astid)
                if os.path.isfile(filename):
                    frames.append([filename, float(x), float(y)])
                else:
                    print("Frame does not exists at {}".format(filename))
            elif found_start:
                break

    # Calculate the average x and y
    viewer.set("blink no")

    viewer.set("frame delete all")

    for filename, x, y in frames:
        viewer.set("frame new")
        viewer.set("scale zscale")
        viewer.set("fits %s" % filename)
        viewer.set("pan to %s %s physical" % (x, y))
        viewer.set("zoom to 2")
        viewer.set("crosshair %s %s physical" % (x, y))
        # viewer.set("regions command '{crosshai 100 100 20 # color=red}'")
        # TODO: draw region

    # setup blink
    viewer.set("blink yes")
    viewer.set("blink interval 0.5")


def average(l):
    return sum(l)/len(l)


def frame_file_name(fname, astid):
    return (SOURCE_EXP_DIR.format(asteroid_id=astid)
            + os.path.sep + fname + ".fit")


def reg_subf_frame_file_name(fname, astid, subf):
    return REG_SUBF_FILENAME.format(frame=fname, asteroid_id=astid,
                                    subf=subf)


def subf_frame_file_name(fname, astid, subf):
    return PROCESSED_SUBF_FILENAME.format(frame=fname, asteroid_id=astid,
                                          subf=subf)


def get_chip(f):
    return f.split('.')[0].split('-')[3]


def get_field(f):
    return f.split('.')[0].split('-')[1]


def get_num(f):
    return int(f.split('.')[0].split('-')[0][1:])


if __name__ == '__main__':
    astid = sys.argv[1]

    subf = True
    reg = False
    if len(sys.argv) > 2:
        if sys.argv[2][1] == 'n':
            subf = False
        elif sys.argv[2][1] == 'r':
            subf = True
            reg = True

    view_in_ds9(astid, subf=subf, registered=reg)
