import fnmatch
import os
import sys
import shutil

from astropy.io import fits

from moa_asteroids.photometry import check_images
from moa_asteroids.photometry.calibrate_lightcurve import calibrate_lightcurve
from moa_asteroids import constants

import numpy as np
import pyds9
import csv


# PyDIA Imports
sys.path.append(constants.PYDIA_DIR)
from Code import c_interface_functions as CF
from Code import calibration_functions as cal

USE_GPU = True
if USE_GPU:
    from Code import DIA_GPU as DIA
else:
    from Code import DIA_CPU as DIA


# Auto generated pyDIA_Files
PYDIA_FILE_LIST = [
    'temp.stars', 'temp.phot', 'temp.phot1', 'temp.phot2', 'temp.pst',
    'temp.opst', 'temp.opst2', 'temp.psf.fits', 'temp.psf1.fits',
    'temp.psf2.fits', 'temp.psg', 'temp.psg2', 'temp.psg3',
    'temp.psg5', 'temp.rej', 'temp.rej2', 'temp.sub.fits', 'temp.sub1.fits',
    'temp.sub2.fits', 'temp.opst1', 'temp.opst3', 'temp.rej3',
    'temp.nst', 'temp.stars1', 'ref.mags', 'psf.fits', 'temp.als',
    'temp.als2', 'star_positions', 'seeing', 'images',
    'ref.flux', 'ref.mags.calibrated', 'ref.flux.calibrated']


def standard_parameters():
    """ Generate the standard parameters for difference imaging for
    moa_asteroids """
    params = DIA.DS.Parameters()
    params.use_GPU = USE_GPU

    params.gain = 2.056
    params.readnoise = 7  # in electrons
    params.datekey = 'JDSTART'

    # NOTE(amelia): Allowing SUBFratic shape fitting can allow for some
    # fix in rotation
    # see: http://toros-astro.github.io/ois/

    # NOTE(amelia): I have now seen the settings used by moa and it is
    # sdeg = 2 bdeg = 2 512 * 512 subframes
    # this is from the DIA tools package on the isee website
    # which I think is different from the main pipeline however has
    # settings for a bunch of things
    params.pdeg = 1
    params.sdeg = 2
    params.bdeg = 2

    params.do_photometry = True
    # NOTE(Amelia): this is one of those things that isn't relevent but
    # that pyDIA complains about otherwise
    params.min_ref_images = 1
    params.max_ref_images = 5

    params.subtract_sky = True
    params.sky_subtract_mode = 'default'

    params.iterations = 2

    # This is when linearity breaks on the CCDs
    # see https://link-springer-com.ezproxy.auckland.ac.nz/article/10.1007%2Fs10686-007-9082-5
    params.pixel_max = 40000

    params.psf_fit_radius = 7.0

    params.datekey = 'JDSTART'
    params.mask_radius = 4  # TODO(amelia): Is this too hight?

    return params


def get_field_chip_subf_list(frame_list):
    """ From a list of frames genate a list of unqiue chips and fields that
    those frames pass through """
    chip_field_dict = {}
    for file_ in frame_list:
        run, field, filter, chip, subf = file_.split("-")

        chip_subf = chip + "-" + subf
        if field not in chip_field_dict:
            chip_field_dict[field] = {}
        if chip_subf not in chip_field_dict[field]:
            chip_field_dict[field][chip_subf] = True

    field_chip_list = []

    for field, i in chip_field_dict.items():
        for chip_subf, _ in i.items():
            field_chip_list.append([field, chip_subf])

    return field_chip_list


def make_difference_images(asteroid_id, field, chip, subf,
                           check_existing=True):
    """ Make a set of difference images for a specified chip and field"""

    if (field, chip, subf) in constants.BAD_SUBFRAMES:
        print("Known bad subframe {} skipping".format((field, chip, subf)))
        return

    source_folder = constants.PROCESSED_EXP_DIR.format(asteroid_id=asteroid_id)
    output_folder = constants.DIFFERENCE_IMG_DIR.format(
        asteroid_id=asteroid_id)

    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    reference_source = constants.REFERENCE_SUBF_FORMAT.format(field, chip,
                                                              subf)
    reference_name = "ref-{}-R-{}-{}.fit".format(field, chip, subf)

    # Check for the existence of chip and field files
    files = [f for f in os.listdir(source_folder) if
             fnmatch.fnmatch(f, '*-{}-R-{}-{}.fit'.format(field, chip, subf))]
    if not files:
        print("No Files for {}-R-{}-{}".format(field, chip, subf))
        # No source files, no need for difference imaging
        return

    if check_existing:
        files = [f for f in os.listdir(output_folder) if
                 fnmatch.fnmatch(f,
                                 'ref.mags{}_{}_{}'.format(field, chip, subf))]
        if files:
            print("Difference imaging already done for {} {} {}".format(
                field, chip, subf))
            return

    params = standard_parameters()

    params.loc_data = source_folder
    params.loc_output = output_folder
    params.registration_image = output_folder + "/" + reference_name

    params.name_pattern = '*-{}-R-{}-{}.fit'.format(field, chip, subf)

    # Check that files matching the name pattern exists
    all_files = os.listdir(params.loc_data)
    files = False
    for f in all_files:
        if fnmatch.fnmatch(f, params.name_pattern):
            files = True

    if files is False:
        print("No match for name pattern {}".format(params.name_pattern))
        return

    # Write Parameters to file
    with open(output_folder + "/params_{}_{}_{}.txt".format(field, chip, subf),
              'wb') as f:
        for name in dir(params):
            f.write("{}: {}\n".format(name, getattr(params, name)))

    if not os.path.isdir(output_folder + os.path.sep):
        os.mkdir(output_folder + os.path.sep)

    # Move the reference image to the Output directory

    # If a cropped reference doesn't exist create it
    shutil.copy(reference_source, output_folder + os.path.sep + reference_name)

    DIA.imsub_all_fits(params, reference=reference_name)

    if os.path.exists(output_folder + os.path.sep + "ref.mags"):
        # On occasion pyDIA will reject files due to very poor
        # fwhm, and then not go through the steps for the reference
        # image processing
        try:
            cal.calibrate(params.loc_output)
        except ValueError as e:
            print(e)  # TODO(amelia): Invesitgate and fix this
            print("Error with subframe {}. Continuing".format(
                (field, chip, subf)))

    # rename star positions etc with the chip name
    for fp in PYDIA_FILE_LIST:
        if os.path.exists(output_folder + os.path.sep + fp):
            os.rename(
                output_folder + os.path.sep + fp,
                output_folder + os.path.sep + fp + "{}_{}_{}".format(
                    field, chip, subf))


def perform_photometry_photom_variable_star(asteroid_id, exposure_list=None,
                                            display_mosaic=False,
                                            calibrate=True,
                                            generate_mosaic=False):
    """ Attempt to perform photometry on created difference images.
    With postions given by the positions.py file.
    """
    # For now assume that there is a single field + chip with a reference image
    # in ref.fit inside Output_
    if exposure_list is None:
        exposure_list = check_images.read_positions_file(asteroid_id)

    lightcurve = []

    mosaic_stamps = []
    pmosaic_stamps = []
    mosaic_filenames = []

    source_folder = constants.PROCESSED_EXP_DIR.format(
        asteroid_id=asteroid_id)
    output_folder = constants.DIFFERENCE_IMG_DIR.format(
        asteroid_id=asteroid_id)

    for frame, x, y, filename in exposure_list:
        run, field, filter, chip, subf = frame.split("-")

        reference_name = "ref-{}-R-{}-{}.fit".format(field, chip, subf)

        params = standard_parameters()
        params.loc_data = source_folder
        params.loc_output = output_folder

        # params.registration_image = '160_processed/obj_B15603-gb16-R-3.fit'
        # params.star_file = output_folder + '/star_positions'
        params.registration_image = reference_name

        # Only the single image
        params.name_pattern = frame + ".fit"
        print(frame)

        # NOTE(amelia): make_difference_images will post fix generated
        # files with "{}_{}".format(field, chip)
        # the first step here is to remove that from the files we care about
        # as pyDIA requires known filename to do things with them

        for fp in PYDIA_FILE_LIST:
            filename = output_folder + os.path.sep + fp + "{}_{}_{}".format(
                field, chip, subf)
            if os.path.exists(filename):
                os.rename(filename, output_folder + os.path.sep + fp)

        x0 = float(x)
        y0 = float(y)

        try:
            dates, seeing, round, bgnd, signal, flux, dflux, \
                quality, x0, y0 = CF.photom_variable_star(
                    x0, y0, params, save_stamps=generate_mosaic, locate=False)
        except ValueError as e:
            print("Error: {} for frame {} continuing".format(e, frame))
            # put the files back where they came from
            for fp in PYDIA_FILE_LIST:
                if os.path.exists(output_folder + os.path.sep + fp):
                    os.rename(
                        output_folder + os.path.sep + fp,
                        output_folder + os.path.sep + fp + "{}_{}_{}".format(
                            field, chip, subf))
            continue

        source_exp = constants.PROCESSED_EXP_FILENAME.format(
            frame=frame, asteroid_id=asteroid_id)

        # Get the date
        with fits.open(source_exp) as f_:
            date = (float(f_[0].header['JDSTART']) + 0.5 *
                    float(f_[0].header['EXPTIME'])/86400.0)

        lightcurve.append([date, frame, flux[0], dflux[0]])

        # put the files back where they came from
        for fp in PYDIA_FILE_LIST:
            if os.path.exists(output_folder + os.path.sep + fp):
                os.rename(
                    output_folder + os.path.sep + fp,
                    output_folder + os.path.sep + fp + "{}_{}_{}".format(
                        field, chip, subf))
        if display_mosaic:
            viewer = pyds9.DS9()
            viewer.set("frame delete all")
            viewer.set("frame new")
            viewer.set("frame new")
            viewer.set("fits %s/pmosaic.fits" % output_folder)
            viewer.set("frame next")
            viewer.set("fits %s/mosaic.fits" % output_folder)

            raw_input("")

        if generate_mosaic:
            # pyDIA will generate a mosaic for a normal stars
            # however this is not a normal star and so I am taking
            # each of the generated images and then saving them
            # myself
            # I don't think that this is the best way of doing this
            # but it is the easiest and as this is only for testing
            # purposes that is all I really need to do

            mos = fits.open(output_folder + "/mosaic.fits")
            pmos = fits.open(output_folder + "/pmosaic.fits")
            mosaic_stamps.append(mos[0].data)
            pmosaic_stamps.append(pmos[0].data)
            mosaic_filenames.append(frame)

    if generate_mosaic:
        patch_size = 33  # This is the size of the saved images fromd pyDIA
        nstamps = len(mosaic_stamps)
        stamps_per_row = int(np.sqrt(nstamps))
        nrows = int((nstamps-1)/stamps_per_row) + 1
        mx = stamps_per_row*patch_size
        my = nrows*patch_size
        mosaic = np.ones((my, mx))
        pmosaic = np.ones((my, mx))
        for i in range(nstamps):
            row = int(i/stamps_per_row)
            column = i % stamps_per_row
            x = row*patch_size
            y = column*patch_size
            mosaic[x:x + patch_size, y:y + patch_size] = mosaic_stamps[i]
            pmosaic[x:x + patch_size, y:y + patch_size] = pmosaic_stamps[i]

        hdu = fits.PrimaryHDU(mosaic)
        hdu.header['comment'] = str(mosaic_filenames)
        hdu.writeto(output_folder + '/fullmosaic.fits', overwrite=True)

        hdu = fits.PrimaryHDU(pmosaic)
        hdu.header['comment'] = str(mosaic_filenames)
        hdu.writeto(output_folder + '/fullpmosaic.fits', overwrite=True)

    lightcurve = calibrate_lightcurve(lightcurve, asteroid_id)

    lc_filename = constants.DIFF_LIGHTCURVE_FILENAME.format(
        asteroid_id=asteroid_id)
    with open(lc_filename, "wb") as f:
        writer = csv.writer(f)
        writer.writerows(lightcurve)
    return lightcurve


def make_asteriod_difference_images(asteroid_id):
    """ Make difference images for all images of an asteroid """
    frame_list = []
    with open(constants.TRACK_FILENAME.format(asteroid_id=asteroid_id)) as f:
        for line in f:
            if not(line.startswith("#")):
                frame_list.append(line.split()[0] + "-" + line.split()[7])

    chip_field = get_field_chip_subf_list(frame_list)

    for (field, chip_subf) in chip_field:
        chip, subf = chip_subf.split("-")
        make_difference_images(asteroid_id, field, chip, subf)
        # TODO(amelia): Group reference images calculations for multiple
        # asteriods, this is a long way off like a long long way off
        # like as in don't expect this ever


if __name__ == '__main__':
    asteroid_id = sys.argv[1]

    make_asteriod_difference_images(asteroid_id)
