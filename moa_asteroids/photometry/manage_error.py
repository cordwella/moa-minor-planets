"""
Manage Errors within the photometry process

These are typically errors that occur due to pyDIA.

The process for dealing with this is as follows:
1) Identify the most recent files that are a part of the most recent
subframe to be processed.
2) Delete all of these files 
Note that anything older than the most recent parameter file will be from a
different subframe.

"""

import sys
import os
from moa_asteroids import constants
import glob


def clean_files(asteroid_id, force=False):
    search_dir = constants.DIFFERENCE_IMG_DIR.format(asteroid_id=asteroid_id)
    # remove anything from the list that is not a file (directories, symlinks)
    # thanks to J.F. Sebastion for pointing out that the requirement was a list
    # of files (presumably not including directories)
    ref_files = list(
        filter(os.path.isfile, glob.glob(search_dir + "/ref*.fit")))
    ref_files.sort(key=lambda x: os.path.getmtime(x))

    # most recent
    ref = ref_files[-1]
    delete_after = os.path.getmtime(ref)

    subframe = tuple(x for x in ref.split(".")[0].split("-")[1:]
                     if x is not "R")

    delete_files = [x for x in glob.glob(search_dir + "/*")
                    if (os.path.isfile(x) and
                        os.path.getmtime(x) >= delete_after)]

    for f in delete_files:
        print("To Delete: {}".format(f))

    if force or raw_input("Delete These?")[0].lower() == 'y':
        for f in delete_files:
            print("Deleting: {}".format(f))
            os.remove(f)

    print("Affected subframe {}".format(subframe))


if __name__ == '__main__':
    asteroid_id = sys.argv[1]
    clean_files(asteroid_id)
