import os
import pyds9
import csv
import sys
import numpy as np

from astropy.io import fits
from astropy.stats import sigma_clipped_stats
from astropy.nddata.utils import overlap_slices

from moa_asteroids import constants
from moa_asteroids.util import get_subf_edges

from photutils import DAOStarFinder
from photutils.centroids.core import centroid_com

ref_stars_cache = {}
PERCENT_THRESHOLD = 0.3


def display(files, x, y):
    """ Show a list of files in ds9 """
    viewer = pyds9.DS9()

    viewer.set("frame delete all")
    for file in files:
        viewer.set("frame new")
        viewer.set("fits %s" % file)
        viewer.set('zscale')
        viewer.set("crosshair %s %s physical" % (x, y))
        viewer.set("pan to %s %s physical" % (x, y))
        viewer.set("zoom to 2")


def automatic_collect_positions(asteroid_id, calculate_centriod=True):
    ftrack = open(constants.TRACK_FILENAME.format(asteroid_id=asteroid_id))
    positions = []
    for line in ftrack:
        frame, _, _, _, _, ox, oy, subf, x, y = line.split()[:10]

        x = float(x)
        y = float(y)
        diff_filename = constants.DIFFERENCE_SUBF_FILENAME.format(
            asteroid_id=asteroid_id, frame=frame, subf=subf)
        mask_filename = constants.MASK_SUBF_FILENAME.format(
            asteroid_id=asteroid_id, frame=frame, subf=subf)

        # check if file exists first
        if not os.path.isfile(diff_filename):
            print("Frame does not exists at {}".format(diff_filename))
            continue

        if calculate_centriod:
            # NOTE(amelia): astropy requires python 3
            # so centroid sources has been copied here
            with fits.open(diff_filename) as f:
                data = f[0].data
                x_, y_ = centroid_sources(data, x, y)
                # Positions in fits files start at zero
                # pyDIA is setup to run this this way
                x_ = x_[0] + 1
                y_ = y_[0] + 1

        # Check distance from the edge
        x_start, y_start, x_finish, y_finish = get_subf_edges(subf)
        x_edge = x_finish - x_start
        y_edge = y_finish - y_start

        if (x_ < 16 or y_ < 16 or
                (x_ + 15 > x_edge) or (y_ + 15 > y_edge)):
            print("Center too close to frame edge. "
                  "Excluding this frame.")
            continue

        # Check distance to saturated points
        # in this case a radius of 7, which is the
        # max psf radius in pyDIA
        rad = 3
        try:
            with fits.open(mask_filename) as f:
                data = f[0].data

                c = data[int(y_ - rad):int(y_ + rad),
                         int(x_ - rad):int(x_ + rad)]
                if np.count_nonzero(c == 0):
                    print("Asteroid too close to saturated pixels. "
                          "Excluding this frame.")
                    continue
        except IOError as e:
            print(e)
            continue
        positions.append([frame + "-" + subf, x_, y_,
                          diff_filename])

    return positions


def collect_positions(asteroid_id, calculate_centriod=True,
                      check_centroid=False):
        ftrack = open(constants.TRACK_FILENAME.format(asteroid_id=asteroid_id))
        viewer = pyds9.DS9()
        x_offset = y_offset = 0
        viewer.set("frame delete all")
        viewer.set("frame new")
        viewer.set("frame new")
        viewer.set("frame new")
        viewer.set("frame new")
        viewer.set("tile yes")
        viewer.set("frame first")

        positions = []
        for line in ftrack:
            frame, _, _, _, _, ox, oy, subf, x, y = line.split()[:10]

            x = float(x)
            y = float(y)

            field, chip = frame.split('-')[1], frame.split('-')[3]

            diff_filename = constants.DIFFERENCE_SUBF_FILENAME.format(
                asteroid_id=asteroid_id, frame=frame, subf=subf)
            source_filename = constants.REG_SUBF_FILENAME.format(
                asteroid_id=asteroid_id, frame=frame, subf=subf)
            reference_filename = constants.REFERENCE_SUBF_FORMAT.format(
                field, chip, subf)
            mask_filename = constants.MASK_SUBF_FILENAME.format(
                asteroid_id=asteroid_id, frame=frame, subf=subf)

            if os.path.isfile(diff_filename):
                x += x_offset
                y += y_offset
                viewer.set("frame first")
                viewer.set("fits %s" % reference_filename)
                viewer.set("pan to %s %s physical" % (x, y))
                viewer.set("zoom to 4")
                viewer.set("crosshair %s %s physical" % (x, y))

                viewer.set("frame next")
                viewer.set("fits %s" % source_filename)
                viewer.set("pan to %s %s physical" % (x, y))
                viewer.set("zoom to 4")
                viewer.set("crosshair %s %s physical" % (x, y))

                viewer.set("frame next")
                viewer.set("fits %s" % mask_filename)
                viewer.set("pan to %s %s physical" % (x, y))
                viewer.set("zoom to 4")
                viewer.set("crosshair %s %s physical" % (x, y))

                viewer.set("frame next")
                viewer.set("fits %s" % diff_filename)
                viewer.set("pan to %s %s physical" % (x, y))
                viewer.set("zoom to 4")
                viewer.set("crosshair %s %s physical" % (x, y))

                i = ""
                while len(i) == 0:
                    i = raw_input(
                        "Action ( bad image b, good image good position g):")
                if i[0].lower() == 'g':
                    x_, y_ = viewer.get("crosshair").split()
                    x_ = float(x_)
                    y_ = float(y_)
                    x_offset = x_ - x
                    y_offset = y_ - y
                    print(x_, y_)
                    if calculate_centriod:
                        # NOTE(amelia): requires python 3
                        # hence a different enviroment to running pyDIA
                        with fits.open(diff_filename) as f:
                            data = f[0].data
                            x_, y_ = centroid_sources(
                                data, x_, y_)
                            # Positions in fits files start at zero
                            x_ = x_[0] + 1
                            y_ = y_[0] + 1

                        if check_centroid:
                            viewer.set(
                                "crosshair %s %s physical" % (x_, y_))
                            i = ""
                            while len(i) == 0:
                                i = raw_input(
                                    "Centriod okay (b=bad):")
                            if i[0] == 'b':
                                continue

                    # Check distance from the edge
                    x_start, y_start, x_finish, y_finish = get_subf_edges(subf)
                    x_edge = x_finish - x_start
                    y_edge = y_finish - y_start

                    if (x_ < 16 or y_ < 16 or
                            (x_ + 15 > x_edge) or (y_ + 15 > y_edge)):
                        print("Center too close to frame edge. "
                              "Excluding this frame.")
                        continue

                    # Check distance to saturated points
                    # in this case a radius of 7, which is the
                    # max psf radius in pyDIA
                    rad = 3
                    with fits.open(mask_filename) as f:
                        data = f[0].data

                        c = data[int(y_ - rad):int(y_ + rad),
                                 int(x_ - rad):int(x_ + rad)]
                        if np.count_nonzero(c == 0):
                            print("Asteroid too close to saturated pixels. "
                                  "Excluding this frame.")
                            continue

                    positions.append([frame + "-" + subf, x_, y_,
                                      diff_filename])

            else:
                print("Frame does not exists at {}".format(diff_filename))
        ftrack.close()
        return positions


# From https://github.com/astropy/photutils/blob/master/photutils/
# centroids/core.py
# This function is only avaliable in the python3 version of photutils
# Currently due to the pyDIA requirements this project is python2
def centroid_sources(data, xpos, ypos, box_size=11, footprint=None,
                     error=None, mask=None, centroid_func=centroid_com):
    """
    Calculate the centroid of sources at the defined positions.
    A cutout image centered on each input position will be used to
    calculate the centroid position.  The cutout image is defined either
    using the ``box_size`` or ``footprint`` keyword.  The ``footprint``
    keyword can be used to create a non-rectangular cutout image.
    Parameters
    ----------
    data : array_like
        The 2D array of the image.
    xpos, ypos : float or array-like of float
        The initial ``x`` and ``y`` pixel position(s) of the center
        position.  A cutout image centered on this position be used to
        calculate the centroid.
    box_size : int or array-like of int, optional
        The size of the cutout image along each axis.  If ``box_size``
        is a number, then a square cutout of ``box_size`` will be
        created.  If ``box_size`` has two elements, they should be in
        ``(ny, nx)`` order.
    footprint : `~numpy.ndarray` of bools, optional
        A 2D boolean array where `True` values describe the local
        footprint region to cutout.  ``footprint`` can be used to create
        a non-rectangular cutout image, in which case the input ``xpos``
        and ``ypos`` represent the center of the minimal bounding box
        for the input ``footprint``.  ``box_size=(n, m)`` is equivalent
        to ``footprint=np.ones((n, m))``.  Either ``box_size`` or
        ``footprint`` must be defined.  If they are both defined, then
        ``footprint`` overrides ``box_size``.
    mask : array_like, bool, optional
        A 2D boolean array with the same shape as ``data``, where a
        `True` value indicates the corresponding element of ``data`` is
        masked.
    error : array_like, optional
        The 2D array of the 1-sigma errors of the input ``data``.
        ``error`` must have the same shape as ``data``.  ``error`` will
        be used only if supported by the input ``centroid_func``.
    centroid_func : callable, optional
        A callable object (e.g. function or class) that is used to
        calculate the centroid of a 2D array.  The ``centroid_func``
        must accept a 2D `~numpy.ndarray`, have a ``mask`` keyword and
        optionally an ``error`` keyword.  The callable object must
        return a tuple of two 1D `~numpy.ndarray`\s, representing the x
        and y centroids.  The default is
        `~photutils.centroids.centroid_com`.
    Returns
    -------
    xcentroid, ycentroid : `~numpy.ndarray`
        The ``x`` and ``y`` pixel position(s) of the centroids.
    """

    xpos = np.atleast_1d(xpos)
    ypos = np.atleast_1d(ypos)
    if xpos.ndim != 1:
        raise ValueError('xpos must be a 1D array.')
    if ypos.ndim != 1:
        raise ValueError('ypos must be a 1D array.')

    if footprint is None:
        if box_size is None:
            raise ValueError('box_size or footprint must be defined.')
        else:
            box_size = np.atleast_1d(box_size)
            if len(box_size) == 1:
                box_size = np.repeat(box_size, 2)
            if len(box_size) != 2:
                raise ValueError('box_size must have 1 or 2 elements.')

        footprint = np.ones(box_size, dtype=bool)
    else:
        footprint = np.asanyarray(footprint, dtype=bool)
        if footprint.ndim != 2:
            raise ValueError('footprint must be a 2D array.')

    use_error = False
    # spec = inspect.getfullargspec(centroid_func)
    # if 'mask' not in spec.args:
    #    raise ValueError('The input "centroid_func" must have a "mask" '
    #                     'keyword.')
    # if 'error' in spec.args:
    #    use_error = True

    xcentroids = []
    ycentroids = []
    for xp, yp in zip(xpos, ypos):
        slices_large, slices_small = overlap_slices(data.shape,
                                                    footprint.shape, (yp, xp))
        data_cutout = data[slices_large]

        mask_cutout = None
        if mask is not None:
            mask_cutout = mask[slices_large]

        footprint_mask = ~footprint
        # trim footprint mask if partial overlap on the data
        footprint_mask = footprint_mask[slices_small]

        if mask_cutout is None:
            mask_cutout = footprint_mask
        else:
            # combine the input mask and footprint mask
            mask_cutout = np.logical_or(mask_cutout, footprint_mask)

        if error is not None and use_error:
            error_cutout = error[slices_large]
            xcen, ycen = centroid_func(data_cutout, mask=mask_cutout,
                                       error=error_cutout)
        else:
            xcen, ycen = centroid_func(data_cutout, mask=mask_cutout)

        xcentroids.append(xcen + slices_large[1].start)
        ycentroids.append(ycen + slices_large[0].start)

        return np.array(xcentroids), np.array(ycentroids)


def write_to_positions_file(asteroid_id, positions):
    filename = constants.POSITIONS_FILENAME.format(asteroid_id=asteroid_id)

    with open(filename, 'w') as resultFile:
        wr = csv.writer(resultFile)
        wr.writerows(positions)


def read_positions_file(asteroid_id):
    filename = constants.POSITIONS_FILENAME.format(asteroid_id=asteroid_id)

    with open(filename) as f:
        pos_list = []
        for line in f:
            pos_list.append(line.split(","))
    return pos_list


if __name__ == '__main__':
    asteroid_id = sys.argv[1]
    mode = 'a'
    if len(sys.argv) > 2:
        mode = sys.argv[2]

    if mode == 'm':
        # Manual collect positions
        positions = collect_positions(asteroid_id)
        write_to_positions_file(asteroid_id, positions)

    elif mode == 'a':
        # Automatically check images and collect images
        positions = automatic_collect_positions(asteroid_id)
        write_to_positions_file(asteroid_id, positions)
