import pyds9
import unittest
import os
import sys

from moa_asteroids.photometry import check_images
from moa_asteroids import constants


def display(files):
    """ Show a list of files in ds9 """
    viewer = pyds9.DS9()

    viewer.set("frame delete all")
    for file in files:
        viewer.set("frame new")
        viewer.set("fits %s" % file)
        viewer.set("zoom to fit")
        viewer.set('zscale')


class TestImageCheck(unittest.TestCase):
    # NOTE(amelia): This requires subframes of certain images of
    # 157 dejenera to exist and be saved inside the test directory

    # in the future this test will include that however for now I will
    # just give the list of frames requried, these will be accessable
    # on a different file system, which then will have to be downloaded
    # to the test data directory

    def test_good_image_check(self):
        frames = ['B1510-gb7-R-2-2_2', 'B1709-gb3-R-9-2_6',
                  'B1235-gb11-R-10-0_4', 'B1625-gb3-R-9-1_3']

        for frame in frames:
            _, field, _, chip, subf = frame.split("-")
            reference_filename = constants.REFERENCE_SUBF_FORMAT.format(
                field, chip, subf)

            filename = (constants.TEST_DATA_DIR + os.path.sep +
                        "r_" + frame + ".fit")

            self.assertTrue(check_images.image_is_good(
                filename, reference_filename))

    def test_bad_image_check(self):
        frames = ['B1245-gb11-R-10-0_6', 'B1714-gb3-R-9-2_6',
                  'B1615-gb3-R-9-1_3']

        for frame in frames:
            _, field, _, chip, subf = frame.split("-")
            reference_filename = constants.REFERENCE_SUBF_FORMAT.format(
                field, chip, subf)

            filename = (constants.TEST_DATA_DIR + os.path.sep +
                        "r_" + frame + ".fit")

            self.assertFalse(check_images.image_is_good(
                filename, reference_filename))

    def test_automatic_collect_positions(self):
        # TODO(amelia):
        # Currently this doesn't exist as this requires a large number of
        # files to exist, and im not wanting to write full tests
        pass


if __name__ == '__main__':
    unittest.main()
