from moa_asteroids.constants import XMAX, YMAX, BORDER, SUBFRAME_SIZE


def get_subf_and_coordinates(x, y):

    # find subf from integer division
    # from subf find the edges using the get_subf_edges command
    # then x = x - xstart y = y - y start

    row = int(x)//SUBFRAME_SIZE
    column = int(y)//SUBFRAME_SIZE

    subf = "{}_{}".format(row, column)

    x_start, y_start, _, _ = get_subf_edges(subf)
    x = x - x_start
    y = y - y_start

    return subf, x, y


def get_subf_edges(subf):
    # subf naming {row}_{column}
    # starting at zero
    row, column = subf.split("_")
    row = int(row)
    column = int(column)
    x_start = row * SUBFRAME_SIZE
    x_finish = (row + 1) * SUBFRAME_SIZE

    y_start = column * SUBFRAME_SIZE
    y_finish = (column + 1) * SUBFRAME_SIZE

    if x_start != 0:
        x_start -= BORDER
    if y_start != 0:
        y_start -= BORDER
    if x_finish != XMAX:
        x_finish += BORDER
    if y_finish != YMAX:
        y_finish += BORDER

    return (x_start, y_start, x_finish, y_finish)
