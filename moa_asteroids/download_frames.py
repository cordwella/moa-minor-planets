#! /usr/bin/env python


"""
Download frames for a given asteriod

Will save into the data/{asteriod_id} directory
Will not download data that already exists
Will flatten the directory structure
Will only download R bands (currently)
"""
from moa_asteroids.setup_bucket import MOA_DB
from moa_asteroids.constants import TRACK_FILENAME, SOURCE_EXP_DIR

from moa_asteroids import stats

import sys
import os

MIN_PASS_LENGTH = 1


def download_asteriod_frames(asteroid_id):
    # Get frame numbers from the track.dat file

    trackf = open(TRACK_FILENAME.format(asteroid_id=asteroid_id))
    folder = SOURCE_EXP_DIR.format(asteroid_id=asteroid_id)

    if not os.path.exists(folder):
        os.makedirs(folder)

    for line in trackf:
        frame = line.split(" ")[0]
        download_frame(frame, folder)


def download_pass(asteroid_id, pass_):
    folder = SOURCE_EXP_DIR.format(asteroid_id=asteroid_id)

    if not os.path.exists(folder):
        os.makedirs(folder)

    for frame in pass_.frames:
        download_frame(frame, folder)


def download_frame(frame, savefolder):
    """ Download a given frame from the MOA_DB

    frame: string name of the frame eg: B23614-gb21-R-3
    savefolder: string, where to save the frame to
    """

    run, field, filter, chip = frame.split('-')

    if field.startswith('gb'):
        galaxy = 'GB'
    else:
        galaxy = field[:3].upper()

    folder = "{}{:02d}{:02d}".format(
        galaxy.upper(), int(field[2:]), int(chip))

    filename = "{}/fit/{}/{}/{}.fit".format(galaxy, folder, filter, frame)
    key = MOA_DB.get_key(filename)
    if key:
        key.get_contents_to_filename("{}/{}.fit".format(savefolder, frame))
    else:
        print("Could not find {}".format(filename))


if __name__ == '__main__':
    astid = sys.argv[1]

    passes = stats.get_passes(astid, time=False)
    for p in passes:
        if p.frame_length() >= MIN_PASS_LENGTH:
            download_pass(astid, p)
    # download_asteriod_frames(astid)
