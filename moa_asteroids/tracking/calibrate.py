#! /usr/bin/env python3
#
# Calculate astrometry
#
import os

from where import find_cooridinates_from_chip_position

PREFIX = '/mnt/sata1/iabond/MOA2proc'
OBJPATH = PREFIX + '/diphot'
CALPATH = PREFIX + '/diphot_cal'


def doone(srcfile, destfile):

    frame = srcfile.split(os.sep)[-1].split('.')[0]
    run, field, colour, chip = frame.split('-')

    fmt = '%5d %12.4f %12.4f %16.10f %16.10f %8.2f\n'
    fin = open(srcfile)
    fout = open(destfile, 'w')
    for line in fin:
        if line.startswith('#'):
            continue
        items = line.split()
        idnum = int(items[0])
        xccd = float(items[1])
        yccd = float(items[2])
        h = float(items[3])
        ra, dec = find_cooridinates_from_chip_position(field, chip, xccd, yccd)
        fout.write(fmt % (idnum, xccd, yccd, ra, dec, h))

    fout.close()
    fin.close()


def main():

    files = os.listdir(OBJPATH)
    files.sort(key=lambda filename: int(filename.split('-')[0].lstrip('B')))
    ntotal = len(files)

    count = 0
    for name in files:
        # For each difference image write out the diphot with additional ra and dec
        count += 1
        fullname = os.path.join(OBJPATH, name)
        destfile = os.path.join(CALPATH, name)
        if os.path.exists(destfile):
            continue

        if count % 10000 == 0:
            print(fullname, destfile, count, '/', ntotal)
        try:
            doone(fullname, destfile)
        except Exception, e:
            fout = open('error.log', 'a')
            fout.write(fullname + '\n')
            fout.write(str(e) + '\n')
            fout.close()
            print(str(e))


if __name__ == '__main__':

    main()
