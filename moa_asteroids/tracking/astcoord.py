#! /usr/bin/env python3
#
'''
Look up RA and DEC for known asteroid at given date
'''
import math

import mpconnect
import geom
import jdate


def get_asteroid_cooridinates(astid, jd):
    year, month, day = jdate.jd2cal(jd)
    iday = int(math.floor(day))
    utoff = 24 * (day - iday)
    ndays = 1
    daystep = 1

    datestart = '{:d}-{:d}-{:d}'.format(year, month, iday)

    res = mpconnect.mpeph(astid, datestart, utoff, ndays, daystep)
    items = res.split()
    ras = ':'.join([items[4], items[5], items[6]])
    decs = ':'.join([items[7], items[8], items[9]])

    delta = items[10]
    r = items[11]
    phi = items[13]
    V = items[14]
    return 15*geom.sex2dec(ras), geom.sex2dec(decs), delta, r, phi, V


if __name__ == '__main__':
    astid = '1'
    jd = 2455476.88486
    get_asteroid_cooridinates(astid, jd)
