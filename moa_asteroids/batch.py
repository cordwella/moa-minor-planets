#! /usr/bin/env python3
#

from moa_asteroids.tracking import make_ephem
from moa_asteroids.tracking import track_in_moa
from moa_asteroids.analysis import export
from moa_asteroids import download_frames
from moa_asteroids.processing import process
from moa_asteroids import stats

from moa_asteroids.photometry.run_all import do_all_photometry

import numpy as np
import matplotlib.pyplot as plt


def batch_stats(idlist):
    """ Overall stats for a list of asteriod ids """
    stat_list = []
    asteroid_frames = []

    non_zero_asteroids = 0
    for astid in idlist:
        stat_list.append(stats.base_stats(astid))
        asteroid_frames.append(sum(stat_list[-1][2]))
        if stat_list[-1][0]:
            non_zero_asteroids += 1

    num_passes = [s[0] for s in stat_list]
    length_passes = []
    num_frames = []
    for s in stat_list:
        length_passes += s[1]
        num_frames += s[2]
    num_passes = np.array(num_passes)
    length_passes = np.array(length_passes)
    num_frames = np.array(num_frames)
    asteroid_frames = np.array(asteroid_frames)

    print("Asteroids with passes {}".format(non_zero_asteroids))

    t = np.unique(num_passes)
    x = [np.count_nonzero(num_passes[num_passes == i]) for i in t]

    plt.close('all')
    plt.figure(1)

    plt.bar(t, x, align='center')
    plt.xlabel("Number of Passes")
    plt.ylabel("Number of Minor Planets")
    plt.title("Number of Passes")

    if non_zero_asteroids:
        plt.figure(2)
        plt.hist(num_frames, bins=np.arange(
            num_frames.min(), num_frames.max()+1, 1))
        plt.xlabel("Number of frames in each pass")
        plt.ylabel("Number of passes")
        plt.title("Frames per pass")

        plt.figure(3)
        plt.hist(length_passes, bins=np.arange(
            length_passes.min(), length_passes.max()+1))
        plt.xlabel("Length of pass (days)")
        plt.ylabel("Number of passes")
        plt.title("Length of Passes")

        plt.figure(4)
        plt.hist(asteroid_frames, bins=np.arange(
            asteroid_frames.min(), asteroid_frames.max()+1, 1))
        plt.xlabel("Number of frames")
        plt.ylabel("Number of minor planets")
        plt.title("Number of frames per minor planets")

    plt.show()


def batch(idlist, download=False, process_=False, track=True,
          difference_photometry=False, export_=False):
    """
    Batch process a set of asteroids. Steps other than track require the
    previous steps to have already been completed to work correctly.

    arguments:
    - idlist (required): list of asteroid ids to run this for
    - track (default True): Track the asteroids inside the MOA data
    - download (default False): Download all images of each asteroid
    - process_ (default False): Perform cropping and ccd processing
        on that asteroids exposures (darks/flats)
    - difference_photometry (default False): Create difference images and
        perform photometry for each asteroid on them. If run a second time
        will only complete difference imaging on frames that do not currently
        have difference images.
    - export_ (default False): generate final export file for that asteroid
    """

    for astid in idlist:
        if track:
            make_ephem.write_asteriod_ephemeris(astid)
            # draw_fields.draw_traj_and_field(astid, interactive=False)
            track_in_moa.map_ephem_to_exposures(astid)
        if download:
            download_frames.download_asteriod_frames(astid)
    # batch_stats(idlist)
    for astid in idlist:
        if process_:
            process.process_asteroid_images(astid)
    for astid in idlist:
        if difference_photometry:
            do_all_photometry(astid)
        if export_:
            export.data_munge(astid)


if __name__ == '__main__':
    idlist = [1370, 1371, 1381, 1395, 1402, 1408, 1410, 1414, 1435, 1441, 1442,
             1445, 1454, 1466, 1475, 1485, 1519, 1526, 1538, 1539, 1569, 1571,
             1588, 1610, 1624, 1649, 1668, 1673, 1686, 1698, 1711]
    idlist = [1009,  1053,  1161,  1229,  1371,  1408,  1441,  1466,  1526,
              157,   1624,  1686,  2010,  2118,  2143,  2288,  2353,  2394,
              2520,  2565,  2623,  5073, 1034,  1066,  1169,  1260,  1381,
              1410,  1442,  1475,  1538,  1571,  1649,  1698,  2011,  2126,
              2149,  2314,  2360,  2432,  2526,  2576,  2630,  5081, 1037,
              1138,  1205,  1327,  1395,  1414,  1445,  1485,  1539,  1588,
              1668,  1711,  2043,  2129,  2263,  2341,  2387,  2503,  2538,
              2594,  2631,  1045,  1141,  1228,  1370,  1402,  1435,  1454,
              1519,  1569,  1610,  1673,  2003,  2069,  2133,  2280,  2345,
              2388,  2511,  2554,  2617,  5023]

    # idlist = [220, 244, 249, 254, 262]
    idlist = [str(i) for i in range(2329, 2500)]

    id_with_process = [ 2069]
    idlist = [2118, 2129, 2129, 2143, 2263, 2288, 5032]
    batch([157], track=False,  download=False, process_=False,
          difference_photometry=False, export_=True)
    #batch_stats([157])
