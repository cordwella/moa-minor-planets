# Correct for light time
# Correct for distance changes
# correct for phase angle changes
# save to a different file
# see the book for details
import sys
from astroquery.jplhorizons import Horizons
import numpy as np
from moa_asteroids import constants
import csv


def correct_lightcurve(asteroid_id):
    # open light curve

    fn = constants.DIFF_LIGHTCURVE_FILENAME.format(asteroid_id=asteroid_id)

    adjusted_curve = []
    with open(fn) as curve:
        for line in curve:
            line = line.split(",")
            time = float(line[0])
            if not line[6]:
                continue
            magnitude = float(line[6])
            magnitude_error = float(line[7])

            ast = Horizons(id=asteroid_id, epochs=time)
            ephem = ast.ephemerides()
            # lightime is returned in minutes
            lighttime = ephem['lighttime'][0] / (60 * 24)

            mid_time = time - lighttime  # time of observation at asteroid

            # Calculate the distances
            # distance adjusted magnitude
            # V corrected = V avg - 5 * log 10 ( D * R )
            # D = earth ast distance in AU
            # R = sun ast distance in AU

            ast = Horizons(id=asteroid_id, epochs=mid_time)
            ephem = ast.ephemerides()
            delta = ephem['delta'][0]
            r = ephem['r'][0]

            # assume no uncertainty added from here
            corrected_mag = magnitude - (5 * np.log10(delta * r))

            # adjust for phase angle TODO
            # see pg 197 of practical guide to lightcurve photometry
            G = ephem['G'][0]
            # the best known phase parameter, may not be exactly correct
            # defaults to 0.15
            # G = 0.02

            alpha = ephem['alpha'][0] * 2 * np.pi / 360

            phi1 = np.exp(-3.33 * (np.tan(alpha/2))**(0.63))
            phi2 = np.exp(-1.87 * (np.tan(alpha/2))**(1.22))

            # see page 197 in practical guide to lightcurve photometry

            corrected_mag = corrected_mag + (
                2.5 * np.log10((1-G)*phi1 + (G * phi2)))

            adjusted_curve.append([mid_time, line[1],
                                   corrected_mag, magnitude_error])
            print([mid_time, line[1], corrected_mag, magnitude_error])


        new_lc_filename = constants.DIFF_LC_REDUCED_FILENAME.format(
            asteroid_id=asteroid_id)
        with open(new_lc_filename, "wb") as f:
            writer = csv.writer(f)
            writer.writerows(adjusted_curve)


if __name__ == '__main__':
    asteroid_id = sys.argv[1]
    correct_lightcurve(asteroid_id)
