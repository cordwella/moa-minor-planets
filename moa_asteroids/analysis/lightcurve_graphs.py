import numpy as np
from matplotlib import pyplot as plt
from moa_asteroids import constants
import os
import sys
"""Generate and save graphs of the calculated (and calibrated) lightcurve"""


def plot_lightcurve(asteroid_id, object_name=None, fmt='o', show=False,
                    start_date=None, end_date=None,
                    limits=None, reduced=False, uncertain=False):
    if reduced:
        fn = constants.DIFF_LC_REDUCED_FILENAME.format(asteroid_id=asteroid_id)
    else:
        fn = constants.DIFF_LIGHTCURVE_FILENAME.format(asteroid_id=asteroid_id)

    if uncertain:
        fn = constants.DIFF_LC_REDUCED_UN_FILENAME.format(
            asteroid_id=asteroid_id)

    with open(fn) as f:
        lc = []
        for line in f:
            line = line.split(',')
            if line[-2]:
                lc.append([float(line[0]), float(line[-2]), float(line[-1])])
    data = np.array(lc)

    if start_date:
        data = data[data[:, 0] >= start_date]
    if end_date:
        data = data[data[:, 0] <= end_date]

    plt.errorbar(data[:, 0], data[:, 1], data[:, 2], fmt=fmt)
    if limits:
        plt.gca().set_ylim(limits)
    plt.gca().invert_yaxis()
    if object_name is None:
        object_name = asteroid_id
    plt.title("Light curve for {}".format(object_name))
    plt.xlabel("Time (Julian Days)")
    if reduced:
        plt.ylabel("Reduced I Magnitudes")
    else:
        plt.ylabel("I Magnitudes")
    plt.savefig(
        constants.ASTEROID_DIR.format(
            asteroid_id=asteroid_id) + "/lightcurve.png",
        bbox_inches='tight')
    if show:
        plt.show()


def plot_lightcurve_timewrap(
        asteroid_id, period, object_name=None, show=False,
        start_date=None, end_date=None, offset=0, fmt='o',
        limits=None, reduced=False, uncertain=False):
    if reduced:
        fn = constants.DIFF_LC_REDUCED_FILENAME.format(asteroid_id=asteroid_id)
    else:
        fn = constants.DIFF_LIGHTCURVE_FILENAME.format(asteroid_id=asteroid_id)

    if uncertain:
        fn = constants.DIFF_LC_REDUCED_UN_FILENAME.format(
            asteroid_id=asteroid_id)

    with open(fn) as f:
        lc = []
        for line in f:
            line = line.split(',')
            if line[-2]:
                lc.append([float(line[0]), float(line[-2]), float(line[-1])])
    data = np.array(lc)

    if start_date:
        data = data[data[:, 0] >= start_date]
    if end_date:
        data = data[data[:, 0] <= end_date]

    dates = data[:, 0] * 24
    time_wrap = np.remainder(dates + offset, period)
    time_wrap = time_wrap/period

    plt.errorbar(time_wrap, data[:, 1], data[:, 2], fmt=fmt)
    plt.gca().set_ylim(limits)
    plt.gca().invert_yaxis()
    if object_name is None:
        object_name = asteroid_id
    plt.title("Period wrapped light curve for {}: Period {}".format(
        object_name, period))
    plt.xlabel("Fraction of period")
    if reduced:
        plt.ylabel("Reduced I Magnitudes")
    else:
        plt.ylabel("I Magnitudes")
    plt.savefig(
        constants.ASTEROID_DIR.format(
            asteroid_id=asteroid_id) + "lightcurve-timewrap.png",
        bbox_inches='tight')
    if show:
        plt.show()


if __name__ == '__main__':
    if len(sys.argv) == 3:
        plot_lightcurve_timewrap(sys.argv[1], float(sys.argv[2]),
                                 show=True, reduced=False)
    else:
        plot_lightcurve(sys.argv[1], show=True, reduced=False)
