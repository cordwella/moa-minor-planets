import sys
from moa_asteroids import constants
import csv

UNCERTAINTY_MULTIPLIER = 3
UNCERTAINTY_ADD = 0.05


def correct_lightcurve_uncertainties(asteroid_id):
    # open light curve

    fn = constants.DIFF_LC_REDUCED_FILENAME.format(asteroid_id=asteroid_id)

    adjusted_curve = []
    with open(fn) as curve:
        for line in curve:
            line = line.split(",")
            magnitude = float(line[2])
            magnitude_error = float(line[3])

            magnitude_error = (magnitude_error * UNCERTAINTY_MULTIPLIER)
            magnitude_error = magnitude_error + UNCERTAINTY_ADD

            adjusted_curve.append([line[0], line[1],
                                   magnitude, magnitude_error])
            print([line[0], line[1], magnitude, magnitude_error])

        new_lc_filename = constants.DIFF_LC_REDUCED_UN_FILENAME.format(
            asteroid_id=asteroid_id)
        with open(new_lc_filename, "wb") as f:
            writer = csv.writer(f)
            writer.writerows(adjusted_curve)


if __name__ == '__main__':
    asteroid_id = sys.argv[1]
    correct_lightcurve_uncertainties(asteroid_id)
