import sys
import os
import numpy as np
import pandas as pd
from astropy.io import fits


from moa_asteroids import constants


def data_munge(asteroid_id):
    """
    Do a final set of data munging for given asteroids and report them into a
    single file that can be exported and placed in an analysis pipeline.

    Output file format:
    "frame, time, exp_time, RA, DEC, framex, framey, subframe, subframex, "
    "subframey, complete, status, adjusted_time, delta, r, alpha, "
    "cropframe, count, counterr, instrum_mag, instrum_magerr, "
    "skymag, skymagerr"

    This will also include files that do not have associated photometry
    so that analysis on the missing photometry can be done

    See page 2 on research notebook #1

    """
    tracking_file = open(constants.TRACK_FILENAME.format(
        asteroid_id=asteroid_id))
    photometry_file = open(constants.DIFF_LIGHTCURVE_FILENAME.format(
        asteroid_id=asteroid_id))

    track_data = []
    p_line = None
    p_frame = None
    photometry_data = []
    status_list = []
    orbit_param = []

    for line in tracking_file:
        li = line.split()
        track_data.append(li[:10])
        frame = li[0]
        subframe = li[7]

        full_frame = frame + "-" + subframe

        try:
            delta, r, alpha = li[10:13]
            # speed of light in AU/julian day
            c_au_jd = 173.14463

            # adjust for the time taken for the light to go from the asteroid
            # to earth
            lighttime = float(delta)/c_au_jd

            # time of observation at asteroid
            mid_time = float(li[1]) - lighttime

            orbit_param.append([mid_time, r, delta, alpha])
        except ValueError:
            print("Error old index file in use. No orbital parameters.")
            orbit_param.append([None, None, None, None])

        if p_line is None:
            p_line = photometry_file.readline().rstrip().split(",")
            if len(p_line) <= 1:  # end of line file with nothing in it
                p_frame = None
            else:
                p_frame = p_line[1]

        if full_frame == p_frame:
            # update for next check
            # check for complete
            if p_line[6]:
                status_list.append([True, 'COMPLETE'])
            else:
                # check for saturation

                x = float(li[8])
                y = float(li[9])

                mask_filename = constants.MASK_SUBF_FILENAME.format(
                    asteroid_id=asteroid_id, frame=frame, subf=subframe)

                rad = 3
                with fits.open(mask_filename) as f:
                    data = f[0].data

                    c = data[int(y - rad):int(y + rad),
                             int(x - rad):int(x + rad)]

                    if np.count_nonzero(c == 0):
                        print("saturated pixels")
                        status_list.append([False, 'Saturated Pixels'])
                    else:
                        status_list.append([False, 'No on sky photometry'])

            photometry_data.append(p_line[1:])

            p_line = None
        else:
            status = None

            # find substatus
            photometry_data.append([None] * 7)

            run, field, filter, chip, subf = full_frame.split("-")

            subframe_details = (field, chip, subf)

            source_file = (
                constants.PROCESSED_EXP_DIR.format(asteroid_id=asteroid_id)
                + os.path.sep + full_frame + ".fits")

            if subframe_details in constants.BAD_SUBFRAMES:
                status = "Known Bad Subframe"
            elif not os.path.isfile(source_file):
                status = "No Source File"
            else:
                status = "Unknown"

            status_list.append([False, status])

    track_data = np.array(track_data)
    status_list = np.array(status_list)

    orbit_param = np.array(orbit_param).astype(float)

    tracking_file.close()
    photometry_file.close()

    headers = ("frame, time, exp_time, RA, DEC, framex, framey, subframe, subframex, "
               "subframey, complete, status, adjusted_time, delta, r, alpha, "
               "cropframe, count, counterr, instrum_mag, instrum_magerr, "
               "skymag, skymagerr")

    full_data = np.hstack((track_data, status_list, orbit_param,
                           photometry_data))
    d2 = pd.DataFrame(full_data, columns=headers.split(", "))

    print(d2)
    d2.to_csv(constants.FINAL_OUTPUT_FILE.format(asteroid_id=asteroid_id), index=False)


if __name__ == '__main__':
    asteroid_id = sys.argv[1]
    data_munge(asteroid_id)
